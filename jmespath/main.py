import jmespath

data = {
    "persons": [
        { "name": "Ale", "age": 46 },
        { "name": "Marcio", "age": 47 },
        { "name": "Sofia", "age": 9 }
    ]
}

print(jmespath.search("persons[*].age", data))

print(jmespath.search("persons[?name=='Ale'].age", data))

print(jmespath.search("persons[?name=='Sofia']", data))


data1 = {
  "ops": {
    "functionA": {"numArgs": 2},
    "functionB": {"numArgs": 3},
    "functionC": {"variadic": True}
  }
}

print(jmespath.search("ops.*.numArgs", data1))


stock = {
    "stocks": [
        {
            "stock": "MSFT",
            "prices": [
                {
                    "high": 232.49000549316406,
                    "low": 224.25999450683594,
                    "open": 226.74000549316406,
                    "close": 226.72999572753906
                },
                {
                    "high": 235.3699951171875,
                    "low": 229.5399932861328,
                    "open": 231.52999877929688,
                    "close": 232.3800048828125
                }
            ]
        },
        {
            "stock": "DIS",
            "prices": [
                {
                    "high": 96.62999725341797,
                    "low": 93.87000274658203,
                    "open": 96.30999755859375,
                    "close": 94.31999969482422                   
                },
                {
                    "high": 95.81999969482422,
                    "low": 93.62999725341797,
                    "open": 94.20999908447266,
                    "close": 95.81999969482422                   
                }
            ]
        }
    ]
}

print("MSFT: ", jmespath.search("stocks[?stock=='MSFT']", stock))
print("MSFT high: ", jmespath.search("stocks[?stock=='MSFT'].prices[].[high, low]", stock))
print("DIS close: ", jmespath.search("stocks[?stock=='DIS'].prices[].close", stock))


test = {"a": {"b": {"c": {"d": "value"}}}}

print(jmespath.search("a.b.c.d", test))



