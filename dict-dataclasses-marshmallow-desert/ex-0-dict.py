import json
import urllib3


http = urllib3.PoolManager()

def get_activity():
    result = http.request("GET", "https://www.boredapi.com/api/activity")
    return json.loads(result.data)

print(get_activity())