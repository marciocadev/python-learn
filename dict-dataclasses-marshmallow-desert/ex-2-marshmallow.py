import json
from logging import error
from marshmallow.decorators import post_load
import urllib3
from dataclasses import dataclass
from marshmallow import EXCLUDE, fields, post_load, Schema, validate

@dataclass
class Activity:
    activity: str
    participants: int
    price: float

http = urllib3.PoolManager()

class ActivitySchema(Schema):
    activity = fields.Str(required=True)
    participants = fields.Int(
                        required=True,
                        validate=validate.Range(min=1, max=50, error="Participants must be between 1 and 50 people")
                    )
    price = fields.Float(
                    required=True,
                    validate=validate.Range(min=0, max=.5, error="Price must be betwwen $1 and $100")
                )

    @post_load
    def transform_price(self, data, **kwargs):
        data["price"] = data["price"] * 100
        return data


def get_activity():
    result = http.request("GET", "https://www.boredapi.com/api/activity")
    result_dict = json.loads(result.data)
    validate_result = ActivitySchema(unknown=EXCLUDE).load(data=result_dict)
    return Activity(
        activity=validate_result["activity"],
        participants=validate_result["participants"],
        price=validate_result["price"]
    )


print(get_activity())