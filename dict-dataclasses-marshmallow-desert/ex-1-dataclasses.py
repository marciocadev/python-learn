import json
import urllib3
from dataclasses import dataclass


@dataclass
class Activity:
    activity: str
    participants: int
    price: float

http = urllib3.PoolManager()

def get_activity():
    result = http.request("GET", "https://www.boredapi.com/api/activity")
    result_dict = json.loads(result.data)
    return Activity(
        activity=result_dict["activity"],
        participants=result_dict["participants"],
        price=result_dict["price"]
    )


print(get_activity())