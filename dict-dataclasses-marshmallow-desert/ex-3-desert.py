import dataclasses
import json
from logging import error
import urllib3
from dataclasses import dataclass
from marshmallow import EXCLUDE, fields, validate
import desert

@dataclass
class Activity:
    activity: str
    participants: int = dataclasses.field(
        metadata = desert.metadata(
            fields.Int(
                required=True,
                validate=validate.Range(min=1, max=50, error="Participants must be between 1 and 50 people")
            )
        )
    )
    price: float = dataclasses.field(
        metadata = desert.metadata(
            fields.Float(
                required=True,
                validate=validate.Range(min=0, max=.5, error="Price must be betwwen $1 and $100")
            )
        )
    )

    def __post_init__(self):
        self.price = self.price * 100

http = urllib3.PoolManager()

def get_activity():
    result = http.request("GET", "https://www.boredapi.com/api/activity")
    result_dict = json.loads(result.data)
    schema = desert.schema(Activity, meta={"unknown": EXCLUDE})
    return schema.load(result_dict)


print(get_activity())